require([
    "vdapi",
    "TeamsiteHelper",
    "metadataDefaultHandle"
  ],
  function (vdapi, ts, mdh) {
    "use strict";

    vdapi.series().done(function (data) {
      ts("/get_yours/MetaData/model_ref").options(data.series.map(function (serie) {
        return {
          label: [serie.display_year, serie.series_name].join(" "),
          value: serie.series_code
        };
      }));

      IWEventRegistry.addItemHandler("/get_yours/MetaData/model_ref", "onItemChange", mdh(data.series));
    });

  }
);
