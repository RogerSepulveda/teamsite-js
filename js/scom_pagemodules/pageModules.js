define([
    "vdapi",
    "scom_pagemodules/options",
    "TeamsiteHelper",
    "scom_pagemodules/singleDetail",
    "scom_pagemodules/paymentDetails",
    "scom_pagemodules/accessoryDetail",
    "scom_pagemodules/getYoursModule",
    "scom_pagemodules/threeUp",
    "scom_pagemodules/featuresList",
    "scom_pagemodules/specs",
    "scom_pagemodules/compareModule",
    "scom_pagemodules/galleryCarousel",
    "scom_pagemodules/optionList",
    "scom_pagemodules/itemGrid"
  ],
  function(vdapi, options, ts, singleDetail, paymentDetails, accessoryDetail, getYours, threeUp, featuresList, specs, compare, galleryCarousel, optionList, itemGrid) {
    "use strict";

    var D = IWDatacapture;
    var ER = IWEventRegistry;

    function clearSelections(replicantPath) {
      while (D.getItem(replicantPath).deleteInstance(1)) {}
    }

    return function pageModules(conf) {
      var model = ts(conf.model);
      var year = ts(conf.year);
      var replicant = conf.tab;
      if (D.getItem(conf.tab + "/content_blocks/")) {
        replicant = conf.tab + "/content_blocks/content_blocks";
      } else {
        replicant = conf.tab + "/content_block";
      }
      var vpath = conf.tab;

      function replicantHandle(rep) {
        var module = rep.getChildren()[0].getName();

        switch (module.split("/").reverse()[0]) {
          case "single_detail":
            vdapi.features({
              "series_code": model.value(),
              "year": year.value()
            }).done(function(data) {
              singleDetail(module, data);
            });
            break;
          case "payment_details":
            vdapi.features({
              "series_code": model.value(),
              "year": year.value()
            }).done(function(data) {
              paymentDetails(module, data);
            });
            break;
          case "accessory_detail":
            vdapi.accessories({
              "series_code": model.value(),
              "year": year.value()
            }).done(function(data) {
              accessoryDetail(module, data);
            });
            break;
          case "GetYours":
            getYours(module, model.value(), year.value());
            break;
          case "features_list":
            featuresList(module, model.value(), year.value());
            break;
          case "three_up":
            vdapi.accessories({
              "series_code": model.value(),
              "year": year.value()
            }).done(function(data) {
              threeUp(module, data);
            });
            break;
          case "specs":
            vdapi.features({
              "series_code": model.value(),
              "year": year.value()
            }).done(function(data) {
              specs(module, data);
            });
            break;
          case "Compare":
            compare(module, model.value(), year.value());
            break;
          case "option_list":
            vdapi.warranties({
              "series_code": model.value().replace("-", ""),
              "year": year.value()
            }).done(function(data) {
              optionList(module, data);
            });
            break;
          case "item_grid":
            vdapi.warranties({
              "series_code": model.value().replace("-", ""),
              "year": year.value()
            }).done(function(data) {
              itemGrid(module, data);
            });
            break;  
          case "GalleryCarousel":
            galleryCarousel(module, model.value(), year.value());
             break;
        }

      }

      ER.addItemHandler(conf.model, "onItemChange", function(item) {
        if (typeof conf.itemChange === "function") {
          conf.itemChange(item);
        }
        clearSelections(replicant);
      });

      ER.addItemHandler(replicant, "onReplicantAdded", replicantHandle);
      if (D.getItem(conf.tab + "/content_blocks/")) {
        vpath += "/content_blocks/";
      } else {
        vpath += "/content_block/"
      }
      ts(vpath).getChildren().forEach(replicantHandle);

      ER.addFormHandler("onSave", function mandatoryModules() {

        var tab = ts(conf.tab);
        var absentModules = (conf.requiredModules || [])
          .filter(function(module) {
            return tab.find(module).length === 0;
          });

        if (absentModules.length) {
          setTimeout(function() {
            alert("The modules " +
              absentModules.join(", ") +
              " are missing in this page and are required. Please add them.");
          }, 100);
          return false;
        } else {
          if (typeof conf.onSave === "function") {
            return conf.onSave();
          }
          return true;
        }
      });
    };
  }
);