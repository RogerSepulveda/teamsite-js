require([
    "vdapi",
    "TeamsiteHelper",
    "jquery"
  ],
  function(vdapi, ts, $) {
    var D = IWDatacapture;
    var ER = IWEventRegistry;
    var modelFields = {
      ref: D.getItem("/model_comparator/metadata/model_ref"),
      year: D.getItem("/model_comparator/metadata/model_year"),
      slug: D.getItem("/model_comparator/metadata/model_slug")
    };

    var refValue = null;
    var featureList = [];
    var comparisonSpecItemPath = "/model_comparator/menu/competitor_models/competitor_model/comparison_specs";
    var cache = {
      series: []
    };

    if (modelFields.ref.getOptions().length === 1) {
      refValue = modelFields.ref.getOptions()[0].value;
    }

    vdapi.series().done(function(data) {
      data.series.forEach(function(serie) {
        var selected = (serie.series_code === refValue);
        var opt = new Option(
          [serie.display_year, serie.series_name].join(" "),
          serie.series_code,
          selected,
          selected);
        modelFields.ref.addOption(opt);

        if (selected) {
          loadFeatures(serie);
        }
      });
      cache = data;
    });

    function loadFeatures(serie) {
      vdapi.features({
        "series_code": serie.series_code,
        "year": serie.year
      }).done(function(data) {
        featureList = data[0].features.map(function(feature) {
          return {
            label: feature.name,
            value: feature.name
          };
        });

        function replicantHandle(item) {
          ts(item.getChildByName("comparison_specs_label").getName()).options(featureList);
        }

        var i = 1;
        while (competitorModel = D.getItem("/model_comparator/menu/competitor_models/competitor_model[" + i + "]")) {
          competitorModel.getChildren().filter(function(item) {
            return item.getName().indexOf("comparison_specs") > -1;
          }).forEach(function(comparisonSpec) {
            replicantHandle(comparisonSpec);
          });
          i++;
        }
        ER.addItemHandler(comparisonSpecItemPath, "onReplicantAdded", replicantHandle);
        ER.addItemHandler("/model_comparator/menu/competitor_models/competitor_model", "onReplicantAdded", function(item) {
          item.getChildren().filter(function(item) {
            return item.getName().indexOf("comparison_specs") > -1;
          }).forEach(function(comparisonSpec) {
            replicantHandle(comparisonSpec);
          });
        });
      });
    }

    ER.addItemHandler("/model_comparator/metadata/model_ref", "onItemChange", function modelChange(item) {
      var serie;
      var series = cache.series.filter(function(serie) {
        return serie.series_code === item.getOptions()[item.getValue()].value;
      });
      if (series.length) {
        serie = series[0];
        modelFields.year.setValue(serie.year.toString());
        modelFields.slug.setValue(serie.series_code);

        loadFeatures(serie);
      }
    });
  }
);