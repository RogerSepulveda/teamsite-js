define(["TeamsiteHelper"], function(ts) {
  "use strict";

  return function itemGrid(vpath, warranties) {
    var warrantiesList = ts(vpath).find("warranty_vdapi_ref");

    function addOptions(select) {
      if (warranties.Warranty !== undefined && select !== undefined) {
        ts(select.getName()).options(warranties.Warranty.map(function(warranty) {
          return {
            label: warranty.header,
            value: warranty.header
          };
        }));
      }
    }

    function trimSpace(item) {
        item.setValue(item.getValue().trim());
    }    

    function getTeamsitePath(vpath) {
      return vpath.replace(/\[\d\]/, '');
    }

    function replicantHandle(path) {
      return function (item) {
        var warrantyRefs = ts(item.getName()).find("warranty_vdapi_ref");
        warrantyRefs.forEach(function (warrantyRef) {
          addOptions(warrantyRef);
        });        
      }
    }

    function checkSeparator(item) {
      if (item.getChildren().length === 2) {
        var separator = ts(item.getName()).find("separator");
        separator.setRequired(true);
      } else {
        separator.setRequired(false);
      }
    }

    warrantiesList
      .forEach(
        addOptions
      );

    IWEventRegistry.addItemHandler(getTeamsitePath(vpath) + "/grid_item", "onReplicantAdded", replicantHandle());
    IWEventRegistry.addItemHandler(getTeamsitePath(vpath) + "/grid_item/item", "onReplicantAdded", replicantHandle());
    IWEventRegistry.addItemHandler(getTeamsitePath(vpath) + "/grid_item/item/warranty_item", "onReplicantAdded", replicantHandle());    
    IWEventRegistry.addItemHandler(getTeamsitePath(vpath) + "/grid_item/item/editorial_item/title", "onItemChange", trimSpace);
    IWEventRegistry.addItemHandler(getTeamsitePath(vpath) + "/grid_item/item/editorial_item/description", "onItemChange", trimSpace);
    IWEventRegistry.addItemHandler(getTeamsitePath(vpath) + "/grid_item/item/equation/equation_items", "onReplicantAdded", checkSeparator);
    IWEventRegistry.addItemHandler(getTeamsitePath(vpath) + "/grid_item/item/equation/equation_items", "onReplicantDelete", checkSeparator);
    IWEventRegistry.addItemHandler(getTeamsitePath(vpath) + "/grid_item/item/equation/equation_items/icon/icon_path", "onItemChange", trimSpace);
    IWEventRegistry.addItemHandler(getTeamsitePath(vpath) + "/grid_item/item/equation/equation_items/stat/value", "onItemChange", trimSpace);
    IWEventRegistry.addItemHandler(getTeamsitePath(vpath) + "/grid_item/item/equation/equation_items/stat/label", "onItemChange", trimSpace);
  }
});