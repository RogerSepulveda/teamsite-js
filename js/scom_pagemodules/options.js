define([],
  function () {
    "use strict";

    return [{
      label: "2Up",
      value: "/templatedata/SComPageModules/2Up/data/${year}/${model}.xml"
    }, {
      label: "Accessory detail",
      value: "/templatedata/SComPageModules/AccessoryDetail/data/${year}/${model}.xml"
    }, {
      label: "Features list",
      value: "/templatedata/SComPageModules/FeaturesList/data/${year}/${model}.xml"
    }, {
      label: "Get yours",
      value: "/templatedata/SComPageModules/GetYours/data/${year}/${model}.xml"
    }, {
      label: "Model comparator",
      value: "/templatedata/SComPageModules/ModelComparator/data/${year}/${model}.xml"
    }, {
      label: "Model navigation",
      value: "/templatedata/SComPageModules/ModelNavigation/data/${year}/${model}.xml"
    }, {
      label: "Single detail",
      value: "/templatedata/SComPageModules/SingleDetail/data/${year}/${model}.xml"
    }].sort(function (modA, modB) {
      if (modA.label < modB.label) {
        return -1;
      } else if (modA.label === modB.label) {
        return 0;
      } else {
        return 1;
      }
    });
  }
);
