IWEventRegistry.addFormHandler("onFormInit", function () {
  "use strict";

  require([
      "vdapi",
      "metadataDefaultHandle",
      "TeamsiteHelper"
    ],
    function (vdapi, mdh, ts) {
      var modelRef = ts("/modelNavigation/MetaData/metadata/model_ref");
      
      vdapi.series().done(function (data) {
        var options = data.series.map(function (serie) {
          return {
            label: [serie.display_year, serie.series_name].join(" "),
            value: serie.series_code
          };
        });

        modelRef.options(options);

        IWEventRegistry.addItemHandler(modelRef.getName(), "onItemChange", mdh(data.series));
      });
    }
  );
});
