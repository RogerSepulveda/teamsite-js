define(["TeamsiteHelper"], function(ts) {
  "use strict";

  return function optionList(vpath, warranties) {
    var warrantiesList = ts(vpath).find("warranty_vdapi_ref");

    function addOptions(select) {
      if (warranties.Warranty !== undefined && select !== undefined) {
        ts(select.getName()).options(warranties.Warranty.map(function(warranty) {
          return {
            label: warranty.header,
            value: warranty.header
          };
        }));
      }
    }

    function trimSpace(item) {
        item.setValue(item.getValue().trim());
    }    

    function getTeamsitePath(vpath) {
      return vpath.replace(/\[\d\]/, '');
    }

    function replicantHandle(path) {
      return function (item) {
        var warrantyRefs = ts(item.getName()).find("warranty_vdapi_ref");
        warrantyRefs.forEach(function (warrantyRef) {
          addOptions(warrantyRef);
        });        
      }
    }

    warrantiesList
      .forEach(
        addOptions
      );

    IWEventRegistry.addItemHandler(getTeamsitePath(vpath) + "/upgrade_items", "onReplicantAdded", replicantHandle());
    IWEventRegistry.addItemHandler(getTeamsitePath(vpath) + "/upgrade_items/icons", "onReplicantAdded", replicantHandle());
    IWEventRegistry.addItemHandler(getTeamsitePath(vpath) + "/upgrade_items/icons/icon_details", "onReplicantAdded", replicantHandle());
    IWEventRegistry.addItemHandler(getTeamsitePath(vpath) + "/upgrade_items/icon_path", "onItemChange", trimSpace);
    IWEventRegistry.addItemHandler(getTeamsitePath(vpath) + "/upgrade_items/icons/editorial_item/title", "onItemChange", trimSpace);
    IWEventRegistry.addItemHandler(getTeamsitePath(vpath) + "/upgrade_items/icons/editorial_item/description", "onItemChange", trimSpace);
  }
});