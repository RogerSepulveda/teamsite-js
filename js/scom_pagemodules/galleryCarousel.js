define(["TeamsiteHelper"], function (ts) {
  "use strict";

  return function galleryCarousel(module, model, year) {
    IWEventRegistry.addItemHandler(module + "/gallery_carousel", "onReplicantAdded", function setGalleryDcrRef(item) {
      item = ts(item);

      var topx = item
        .find("top_x")
        .forEach(function (item) {
          IWEventRegistry.addItemHandler(item.getName(), "onItemChange", function (item) {
            item = ts(item);

            if (isNaN(item.value()) || +item.value() < 1) {
              item.value("");
            }
          });
        });

      item
        .find("gallery_page_dcr_ref")
        .forEach(function (item) {
          item.value("/templatedata/SComPageTemplates/ModelGalleryPage/data/" + year + "/"+ model + ".xml");
        });
    });
  };
});