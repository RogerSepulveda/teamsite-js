define(["TeamsiteHelper"], function(ts) {
  "use strict";

  function searchSpecRefs(item, specRefList) {
    var specRef = item.getChildByName("spec_vdapi_ref");
    var itemChildren = item.getChildren();

    if (specRef !== undefined) {
      specRefList.push(specRef);
    } else if (itemChildren.length > 0) {
      itemChildren.forEach(function(item) {
        specRefList.concat(searchSpecRefs(item, specRefList));
      });
    }
    return specRefList;
  };

  return function specs(vpath, specs) {
    var specRefList = searchSpecRefs(ts(vpath), []);

    function addOptions(select) {
      ts(select.getName()).options(specs[0].features.map(function(feature) {
        return {
          label: feature.name,
          value: feature.name
        };
      }));
    }

    function getTeamsitePath(vpath) {
      return vpath.replace(/\[\d\]/, '');
    }

    function replicantHandle(path) {
      return function (item) {
        addOptions(item.getChildByName((path || "") + "spec_vdapi_ref"));
      }
    }

    specRefList
      .forEach(
        addOptions
      );

    IWEventRegistry.addItemHandler(getTeamsitePath(vpath) + "/with_key_specs/specs", "onReplicantAdded", replicantHandle());
    IWEventRegistry.addItemHandler(getTeamsitePath(vpath) + "/with_key_specs/key_specs", "onReplicantAdded", replicantHandle());
    IWEventRegistry.addItemHandler(getTeamsitePath(vpath) + "/without_key_specs/specs", "onReplicantAdded", replicantHandle());
  }
});