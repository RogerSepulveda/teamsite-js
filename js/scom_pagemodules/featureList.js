require([
    "vdapi",
    "TeamsiteHelper",
    "metadataDefaultHandle"
  ],
  function (vdapi, ts, mdh) {
    "use strict";

    var ER = IWEventRegistry;
    var loadCat = function(item) {
      vdapi.features({
        "series_code": ts("/features_list/MetaData/model_ref").value(),
        "year": ts("/features_list/MetaData/model_year").value()
      }).done(function (features) {
        var options = features[0].features.map(function (feature) {
            return feature.categories;
          }).reduce(function (categories, currentCategories) {
            return categories.concat(currentCategories.filter(function (category) {
              return !categories.some(function (cat) {
                return cat.code === category.code;
              });
            }));
          }, [])
          .map(function (category) {
            return {
              label: category.name,
              value: category.code
            };
          });

        ts(item.getChildByName("category_title").getName())
          .options(options);
      });
    }

    ER.addFormHandler("onFormInit", function() {

      vdapi.series().done(function (data) {
        var handleHiddenFields = mdh(data.series);

        ts("/features_list/MetaData/model_ref")
          .options(data.series.map(function (serie) {
            return {
              label: [serie.display_year, serie.series_name].join(" "),
              value: serie.series_name
            };
          }));

        if(ts("/features_list/MetaData/model_ref").value()) {
          ts("/features_list/Category/category").getChildren().forEach(function (child) {
            loadCat(child);
          });
        }

        ER.addItemHandler("/features_list/MetaData/model_ref", "onItemChange", function (item) {
          handleHiddenFields(item);
          while(ts("/features_list/Category/category").deleteInstance(1)){}
        });

        ER.addItemHandler("/features_list/Category/category", "onReplicantAdded", function (item) {
          loadCat(item);
        });
      });
    });
  }
);
