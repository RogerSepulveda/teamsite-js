define(["TeamsiteHelper"], function (ts) {
  "use strict";

  return function accessoryDetail(vpath, accessories) {
    ts(vpath + "/accessory_ref")
      .options(accessories[0].accessories.map(function (accessory) {
        return {
          label: accessory.name,
          value: accessory.code
        };
      }));
  };
});
