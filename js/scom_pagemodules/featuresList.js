define(["TeamsiteHelper"], function (ts) {
  "use strict";

  return function (vpath, model, year) {
    var dcrRef = ts(vpath + "/features_list_dcr_ref");
    var value = "/templatedata/SComPageModules/FeaturesList/data/" +
      year + "/" + model + ".xml";

    if (!dcrRef.value()) {
      dcrRef.value(value);
    }

  };
});