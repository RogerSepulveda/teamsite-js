define(["TeamsiteHelper"], function (ts) {
  "use strict";

  return function (vpath, model, year) {
    var dcrRef = ts(vpath + "/get_yours_dcr_ref");
    var value = "/templatedata/SComPageModules/GetYours/data/" +
      year + "/" + model + ".xml";

    if (!dcrRef.value()) {
      dcrRef.value(value);
    }

  };
});
