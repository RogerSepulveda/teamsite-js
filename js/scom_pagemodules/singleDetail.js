define([
    "TeamsiteHelper"
  ],
  function (ts) {
    "use strict";

    return function (vpath, features) {

      function setOptions() {
        ts(vpath + "/feature_item_layout[1]/vdapi_feature[1]/feature_ref")
          .options(features[0].features.map(function (feature) {
            return {
              label: feature.name,
              value: feature.name
            };
          }));
      }

      IWEventRegistry.addItemHandler(vpath + "/feature_item_layout/vdapi_feature", "onReplicantAdded", function (item) {
        if (item.getName().indexOf("feature_item_layout") > -1) {
          setOptions();
        }
      });

      if (IWDatacapture.getItem(vpath + "/feature_item_layout[1]/vdapi_feature[1]/feature_ref")) {
        setOptions();
      }
    };

  }
);
