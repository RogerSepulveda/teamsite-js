define([
    "TeamsiteHelper"
  ],
  function (ts) {
    "use strict";
    var errorMessage = "Can't add more than one large link group";

    return function linkList(module) {
      IWEventRegistry.addItemHandler(
        module.getName() + "/link_group/link_group",
        "onReplicantBeforeAdd",
        function (item) {
          if (module.find("link_list_8row").length > 0) {
            setTimeout(function () {
              alert(errorMessage);
            }, 10);

            return false;
          } else {
            return true;
          }
        }
      );
    };
  }
);
