define([
    "TeamsiteHelper",
    "scom_pagemodules/story_telling/linkList"
  ],
  function (ts, linkList) {
    "use strict";

    function replicantHandle(rep) {
      rep = ts(rep);
      var module = rep.getChildren()[0].getName();

      switch (module.split("/").pop()) {
        case "link_list":
          linkList(ts(module));
          break;
      }
    };

    return function storytellingPageModules(conf) {
      var replicant = conf.tab + "/content_blocks/content_blocks"
      IWEventRegistry.addItemHandler(replicant, "onReplicantAdded", replicantHandle);
      ts(conf.tab + "/content_blocks").getChildren().forEach(replicantHandle);
    };
  }
);
