define(["TeamsiteHelper"], function (ts) {
  "use strict";

  return function (vpath, model, year) {
    var dcrRef = ts(vpath + "/compare_dcr_ref");
    var value = "/templatedata/SComPageModules/ModelComparator/data/" +
      year + "/" + model + ".xml";

    dcrRef.value(value);
  };
});