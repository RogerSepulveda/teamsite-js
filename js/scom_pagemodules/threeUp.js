define(["TeamsiteHelper"], function(ts) {
  "use strict";

  return function threeUp(vpath, accessories) {
    ts(vpath)
      .getChildren()
      .filter(function(child) {
        return child.getName().indexOf("accessory") > -1;
      })
      .map(function(container) {
        return ts(container.getChildByName("accessory_vdapi_ref").getName());
      })
      .forEach(function(select) {
        select.options(accessories[0].accessories.map(function(accessory) {
          return {
            label: accessory.name,
            value: accessory.code
          };
        }));
      });
  }
});
