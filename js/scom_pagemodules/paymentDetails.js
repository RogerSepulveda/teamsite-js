define([
    "TeamsiteHelper",
    "vdapi"
  ], function(ts, vdapi) {
    "use strict";

    return function(vpath, features) {
      ts(vpath)
        .getChildren()
        .filter(function(child) {
          return child.getName().indexOf("payment_details_layout") > -1;
        })
        .map(function(container) {
          return ts(container.getChildByName("detail_spec_vdapi_ref").getName());
        })
        .forEach(function(select) {
          select.options(features[0].features
            .filter(function(feature) {
              return feature.categories.some(function(category) {
                return category.code === "payment_details";
              });
            })
            .map(function(feature) {
              return {
                label: feature.name,
                value: feature.name
              };
            })
          );
        });
    };
});