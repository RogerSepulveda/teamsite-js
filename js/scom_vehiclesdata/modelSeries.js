require([
    "vdapi"
  ],
  function (vdapi) {
    "use strict";

    
    var series = {
      data: null,
      currentModel: null,
      seriesRef: "/modelSeries/ModelInfo/series_ref",
      modelRef: "/modelSeries/ModelInfo/model_ref",
      modelDcr: "/modelSeries/ModelInfo/model_dcr",
      featuresDcr: "/modelSeries/ModelInfo/features_dcr",
    };
    var features = {
      data: null,
      year: null,
      container: "/modelSeries/Features/features",
      featuresRef: function(instanceName) {
        return instanceName + "/features_ref";
      },
    };
  
    vdapi.series().done(function (data) {
      series.data = data;

      var dropdown = IWDatacapture.getItem(series.seriesRef);

      data.series.forEach(function (serie) {
        dropdown.addOption(new Option([serie.display_year, serie.series_name].join(" "), serie.series_code, '', ''));
      });
    });

    IWEventRegistry.addItemHandler(features.container, "OnReplicantAdded", onAddFeature);
    IWEventRegistry.addItemHandler(series.seriesRef, "onItemChange", setModelReferences);
     
    function setModelReferences(item) {
      console.log(item.getName());
      var selected = series.data.series.filter(function findSerie(s) {
        var current = s.series_code;
        var value = item.getOptions()[item.getValue()].value;
        
        return s.series_code === item.getOptions()[item.getValue()].value;
      });
      
      if (selected.length) {
        var seriesData = selected[0];

        series.currentModel = seriesData;
        IWDatacapture.getItem(series.featuresDcr).setValue(encodeURI("templatedata/SComVehiclesData/ModelFeatures/data/" + seriesData.year + "/" + seriesData.series_code + ".xml"));
        IWDatacapture.getItem(series.modelDcr).setValue(encodeURI("templatedata/SComVehiclesData/Model/data/" + seriesData.year + "/" + seriesData.series_code + ".xml"));
        
      }

      clearFeatures();
    }

    function clearFeatures() {
      var container = IWDatacapture.getItem(features.container);

      if (container != null) {
        for (var index = container.getChildren().length; index > 0; index--) {
          container.deleteInstance(index);
        }
      }

      features.data = null;
    }
 
    function onAddFeature(item) {

      if (features.data != null) {
        loadFeatures(item.getName());
      } else {
        var params = {
          "series_code": series.currentModel.series_code,
          "year": series.currentModel.year
        };

        vdapi.features(params).done(function(data) {
          if (Array.isArray(data) && data[0]) {
            features.data = data[0].features;
            loadFeatures(item.getName());
          } else {
            clearData();
          }
        });
      }
      
      return true;
    }
    
    function clearData() {
      features.data = null;
      features.year = null;
    }

    function loadFeatures(containerName) {
      var list = IWDatacapture.getItem(features.featuresRef(containerName));
      
      $.each(features.data, function(index, feature) {
        list.addOption(new Option(feature.name, feature.name, '', ''));
      });
    }
  }
);

