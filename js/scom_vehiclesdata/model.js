function init() {
    console.log("Initializing form api");
    IWEventRegistry.addItemHandler("/model/model_info/model_details/model_ref", "onItemChange", setModelBaseReferences);
    console.log("fds");
    IWEventRegistry.addItemHandler("/model/features_info/feature_details/feature", "OnReplicantAdded", getFeaturesData);    
    console.log("sdfsdfsfsd");
    IWDatacapture.getItem("/model/model_info/model_details/vdapi_model_ref_url").setValue("http://next.staging.ws.toyota.com/CLSREST/api/vehicle_data/series?year=2015&depth=2&key=MtK7p8aB9qmCBObkKlzs&brand=scion");    
    console.log ("Starting to make a call to VDAPI - " + IWDatacapture.getItem("/model/model_info/model_details/vdapi_model_ref_url").getValue());
    
    $.ajax({
      url: IWDatacapture.getItem("/model/model_info/model_details/vdapi_model_ref_url").getValue(),
      type: 'GET',
      dataType: "JSONP",
      success: function(data) {
        //called when successful
        var dropdown = IWDatacapture.getItem("/model/model_info/model_details/model_ref");
        $.each(data.series, function(idx, series) {
            dropdown.addOption(new Option(series.display_year + " " + series.series_name, series.series_id, '', ''));
            IWDatacapture.getItem("/model/model_info/model_details/vdapi_json").setValue(data);
        });
        console.log("VDAPI call successful");
      },
      error: function(e) {
        //called when there is an error
        console.log(e.message);
      }
    });
}

function setModelBaseReferences(item) {
  console.log("Inside setModelBaseReferences");
  $.each(IWDatacapture.getItem("/model/model_info/model_details/vdapi_json").getValue().series, function(idx, series) {
      if (item.getOptions()[item.getValue()].value == series.series_id) {
        IWDatacapture.getItem("/model/model_info/model_details/model_year").setValue(series.display_year);
        IWDatacapture.getItem("/model/model_info/model_details/model_slug").setValue(series.series_code);

        IWDatacapture.getItem("/model/model_info/model_details/features_dcr").setValue("templatedata/SComVehiclesData/ModelFeatures/data/" + series.display_year + "/" + series.series_code + ".xml");
        IWDatacapture.getItem("/model/model_info/model_details/specifications_dcr").setValue("templatedata/SComVehiclesData/ModelFeatures/data/" + series.display_year + "/" + series.series_code + ".xml");
        IWDatacapture.getItem("/model/model_info/model_details/color_dcr").setValue("templatedata/SComVehiclesData/ModelColor/data/" + series.display_year + "/" + series.series_code + ".xml");
        IWDatacapture.getItem("/model/model_info/model_details/accessories_dcr").setValue("templatedata/SComVehiclesData/ModelAccessories/data/" + series.display_year + "/" + series.series_code + ".xml");
      }
  });

  function getFeaturesData(item) {
    IWDatacapture.getItem("/model/model_info/model_details/vdapi_features_ref_url").setValue("http://next.staging.ws.toyota.com/CLSREST/api/vehicle_data/features?year="+IWDatacapture.getItem("/model/model_info/model_details/model_year").getValue()+"&series_code="+IWDatacapture.getItem("/model/model_info/model_details/model_slug")+"&depth=1&key=MtK7p8aB9qmCBObkKlzs&brand=scion");
    
    var dropdown = IWDatacapture.getItem(item.getName() + "/feature_ref");
    console.log("Making VDAPI call for getting features data")
    $.ajax({
      url: IWDatacapture.getItem("/model/model_info/model_details/vdapi_features_ref_url").getValue(),
      type: 'GET',
      dataType: "JSONP",
      success: function(data) {
        //called when successful
        $.each(data.features, function(idx, features) {
            dropdown.addOption(new Option(features.name, features.name, '', ''));
        });
        console.log("VDAPI call successful");
      },
      error: function(e) {
        //called when there is an error
        console.log(e.message);
      }
    });
  }
}

init();
