function init() {

    $.ajax({
        url: 'http://next.staging.ws.toyota.com/CLSREST/api/vehicle_data/series?depth=2&key=MtK7p8aB9qmCBObkKlzs&brand=scion',
        type: 'GET',
        dataType: "JSONP",
        success: function (data) {
            //called when successful
            var dropdown = IWDatacapture.getItem("/modelSpecifications/MetaData/model_ref");
            $.each(data.series, function (idx, series) {
                dropdown.addOption(new Option(series.display_year + " " + series.series_name, series.series_code, '', ''));
            });
            console.log("VDAPI call successful");
        },
        error: function (e) {
            //called when there is an error
            console.log(e.message);
        }
    });
}

init();
