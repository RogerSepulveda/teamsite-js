require([
    "vdapi",
    "TeamsiteHelper",
    "jquery"
  ],
  function(vdapi, ts, $) {
    "use strict";

    var D = IWDatacapture;
    var ER = IWEventRegistry;
    var specificationItemPath = "/competitorModel/specifications/specification";
    var specificationItem = null;

    vdapi.series().done(function(data) {
      var queries = data.series.map(function(serie) {
        return vdapi.features({
          "series_code": serie.series_code,
          "year": serie.year
        });
      });

      $.when.apply($, queries)
        .done(function() {
          var options = [].concat.apply([], arguments).map(function(data) {
            var optionList = [];
            if (data[0] !== undefined && data[0].features !== undefined) {
              data[0].features.forEach(function(feature) {
                optionList = optionList.concat(feature.name);
              });
            }
            return optionList;
          }).reduce(function(featuresList, element) {
            featuresList = featuresList.concat(element);
            return featuresList;
          }, []).filter(function (item, pos, array) {
            return array.indexOf(item) == pos;
          }).map(function (item) {
            return { label: item, value: item };
          });

          function replicantHandle(item) {
            ts(item.getChildByName("specification_title").getName()).options(options);
          }

          var i = 1;
          while (specificationItem = D.getItem("/competitorModel/specifications[1]/specification[" + i + "]")) {
            replicantHandle(specificationItem);
            i++;
          }
          ER.addItemHandler(specificationItemPath, "onReplicantAdded", replicantHandle);

        });
    });

  });