var vdapi_host = "http://next.staging.ws.toyota.com/CLSREST/api/vehicle_data";
var models = {
    data: null,
    fields: {
        model_ref: "/model_color/MetaData/model_ref",
        vdapi_ref_url: vdapi_host + "/series?depth=2&key=MtK7p8aB9qmCBObkKlzs&brand=scion",
        name: null,
        year: null
    }
};

var colors = {
    data: null,
    fields: {
        color: "/model_color/colors/exterior_colors/exterior_color",
        color_ref: function(instanceName) {
            return instanceName + "/color_ref";
        },
        color_image_path: function(instanceName) {
            return instanceName + "/color_image_path";
        },
        color_image_title: function(instanceName) {
            return instanceName + "/color_image_title";
        },
        color_hex_code: function(instanceName) {
            return instanceName + "/color_hex_code";
        },
        efc_id: function(instanceName) {
            return instanceName + "/efc_id";
        },
        vdapi_ref_url: vdapi_host + "/colors?brand=scion&key=MtK7p8aB9qmCBObkKlzs&",
    }
};

function init() {
    IWEventRegistry.addItemHandler(colors.fields.color, "OnReplicantAdded", handleReplicantAdded);
    IWEventRegistry.addItemHandler(models.fields.model_ref, "onItemChange", setModelBaseReferences);
    console.log(models.fields.model_ref);
    $.ajax({
        url: models.fields.vdapi_ref_url,
        type: 'GET',
        dataType: "JSONP",
        success: function (data) {
            //called when successful
            var dropdown = IWDatacapture.getItem(models.fields.model_ref);
            $.each(data.series, function (idx, series) {
                dropdown.addOption(new Option(series.display_year + " " + series.series_name, series.series_code, '', ''));
                models.data = data;
            });
            console.log("VDAPI call successful");
        },
        error: function (e) {
            //called when there is an error
            console.log(e.message);
            models.data = null;
        }
    });
}

function setModelBaseReferences(item) {
  console.log("Inside setModelBaseReferences");
  $.each(models.data.series, function(idx, series) {
      if (item.getOptions()[item.getValue()].value == series.series_code) {
        models.fields.name = series.series_code;
        models.fields.year = series.year + "";
        console.log(models.fields.name + " " + models.fields.year);
      }
  });
  clearColors();
}

function handleColorChange(item) {
    console.log("handleColorChange");
    $.each(colors.data, function(idx, color) {
        if (item.getOptions()[item.getValue()].value == color.name) {
           var color_hex_code = item.getName().replace("color_ref", "color_hex_code");
           var efc_id = item.getName().replace("color_ref", "efc_id");

           IWDatacapture.getItem(color_hex_code).setValue(color.hex_source);
           IWDatacapture.getItem(efc_id).setValue(color.code);
        }
    });
}

function clearColors() {
    var container = IWDatacapture.getItem("/model_color/colors/exterior_colors[1]/exterior_color");
    
    if (container != null) {
        for (var index = container.getChildren().length; index > 0; index--) {
            container.deleteInstance(index);
        }
    }
    colors.data = null;
}

function handleReplicantAdded (item) {
    console.log("handleReplicantAdded: " + item.getName());
    
    if (colors.data != null) {
        loadColors(item.getName());
    } else {
        $.ajax({
            url: colors.fields.vdapi_ref_url + "&series_code=" + models.fields.name + "&year=" + models.fields.year,
            type: 'GET',
            dataType: "JSONP",
            success: function (data) {
                console.log("Specs request: success.");
                if (Array.isArray(data) && data[0] && data[0].colors && data[0].colors.exteriors) {
                    colors.data = data[0].colors.exteriors;
                    loadColors(item.getName());
                } else {
                    console.log("Invalid response.");
                    console.log(data);
                    colors.data = null;
                }
            },
            error: function (e) {
                //called when there is an error
                console.log(e.message);
                colors.data = null;
            }
        });
    }
    return true;
}

function loadColors(containerName) {
    console.log("loadColors");
    var list = IWDatacapture.getItem(colors.fields.color_ref(containerName));
    $.each(colors.data, function(index, color) {
        list.addOption(new Option(color.name, color.name, '', ''));
    });
    IWEventRegistry.addItemHandler(colors.fields.color_ref(containerName), "onItemChange", handleColorChange);
}

init();
