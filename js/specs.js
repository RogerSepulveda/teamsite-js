define([
        "vdapi", "jquery"
    ],
    function (vdapi, $) {
        "use strict";

        var specs = {};
        var cache = {
            selectedSeries: null,
            currentSeries: null,
            specs: {},
            specRefList: []
        };

        var handleReplicantAdded = function (item) {
            console.log("handleReplicantAdded: " + item.getName());
            cache.specRefList = [];
            var specRefList = searchSpecRefs(item);
            populateSpecs(specRefList);
            return true;
        };

        var searchSpecRefs = function (item) {
            var specRef = item.getChildByName("spec_ref") || item.getChildByName("spec_vdapi_ref");
            var itemChildren = item.getChildren();

            if (specRef !== undefined) {
                cache.specRefList.push(specRef);
            } else if (itemChildren.length > 0) {
                itemChildren.forEach(function (item) {
                    cache.specRefList.concat(searchSpecRefs(item));
                });
            }
            return cache.specRefList;
        };

        specs.load = function (series, containerElementPath, selected) {
            var containerElement = IWDatacapture.getItem(containerElementPath);
            IWEventRegistry.addItemHandler(containerElementPath, "OnReplicantAdded", handleReplicantAdded);
            var params = {
                "series_code": series.series_code,
                "year": series.year
            };

            // Load specs
            vdapi.features(params).done(function (data) {
                if (data && data[0] && data[0].features && containerElement) {
                    cache.specs[series.series_code] = data[0].features;
                    cache.currentSeries = series.series_code;
                    if (selected) {
                        cache.selectedSeries = series.series_code;
                    }

                    containerElement.getChildren().forEach(function (item) {
                        handleReplicantAdded(item);
                    });
                }
            });
        };

        var populateSpecs = function (refList) {
            refList.forEach(function (ref) {
                populateSpec(ref);
            });

        };

        var populateSpec = function (ref) {
            console.log("self.populateSpecs: " + ref.getName());

            var options = [];
            var selectedIndex = null;

            if (!cache.specs[cache.currentSeries]) {
                return;
            }
            $.each(cache.specs[cache.currentSeries], function (index, spec) {
                var selected = false;
                if (ref.getOptions().length === 1 && cache.selectedSeries === cache.currentSeries) {
                    var value = ref.getOptions()[0].value;

                    if (spec.name === value) {
                        selectedIndex = index;
                        selected = true;
                    }
                }

                var opt = new Option(spec.name, spec.name, selected, selected);
                options.push(opt);
            });

            ref.setOptions(options);
            ref.setValue(selectedIndex);
        };

        return specs;
    }
);


