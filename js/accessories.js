define([
        "vdapi", "jquery"
    ],
    function (vdapi, $) {
        "use strict";

        var accessories = {};
        var cache = {
            selectedSeries: null,
            currentSeries: null,
            accessories: {},
            accessoryRefList: []
        };

        var handleReplicantAdded = function (item) {
            console.log("handleReplicantAdded: " + item.getName());
            cache.accessoryRefList = [];
            var accessoryRefList = searchAccessoryRefs(item);
            populateAccessories(accessoryRefList);
            return true;
        };

        var searchAccessoryRefs = function (item) {
            var accessoryRef = item.getChildByName("accessory_ref") || item.getChildByName("accessory_vdapi_ref");
            var itemChildren = item.getChildren();

            if (accessoryRef !== undefined) {
                cache.accessoryRefList.push(accessoryRef);
            } else if (itemChildren.length > 0) {
                itemChildren.forEach(function (item) {
                    cache.accessoryRefList.concat(searchAccessoryRefs(item));
                });
            }
            return cache.accessoryRefList;
        };

        accessories.load = function (series, containerElementPath, selected) {
            var containerElement = IWDatacapture.getItem(containerElementPath);
            IWEventRegistry.addItemHandler(containerElementPath, "OnReplicantAdded", handleReplicantAdded);
            var params = {
                "series_code": series.series_code,
                "year": series.year
            };

            // Load accessories
            vdapi.accessories(params).done(function (data) {
                if (data && data[0] && data[0].accessories) {
                    cache.accessories[series.series_code] = data[0].accessories;
                    cache.currentSeries = series.series_code;
                    if (selected) {
                        cache.selectedSeries = series.series_code;
                    }

                    containerElement.getChildren().forEach(function (item) {
                        handleReplicantAdded(item);
                    });
                }
            });
        };

        var clearElements = function (containerElement) {
            console.log("self.clearElements");
            if (containerElement != null) {
                for (var index = containerElement.getChildren().length; index > 0; index--) {
                    containerElement.deleteInstance(index);
                }
            }
        };

        var populateAccessories = function (refList) {
            refList.forEach(function (ref) {
                populateAccessory(ref);
            });

        };

        var populateAccessory = function (ref) {
            console.log("self.populateAccessories: " + ref.getName());

            var options = [];

            if (!cache.accessories[cache.currentSeries]) {
                return;
            }
            $.each(cache.accessories[cache.currentSeries], function (index, accessory) {
                var selected = false;
                if (ref.getOptions().length === 1 && cache.currentSeries === cache.selectedSeries) {
                    var value = ref.getOptions()[0].value;

                    if (accessory.code === value) {
                        selected = true;
                    }
                }

                var opt = new Option(
                    accessory.name,
                    accessory.code,
                    "",
                    selected
                );
                options.push(opt);
            });

            ref.setOptions(options);
        };

        return accessories;
    }
);


