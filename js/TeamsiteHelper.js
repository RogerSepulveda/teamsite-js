define([],
  function () {
    "use strict";

    /**
     * Extends a teamsite item with utility functions.
     * @param  {IWDatacapture.getItem}
     * @return {HUGEItem}
     */
    function extend(item) {

      // The object to be extended and then returned.
      item = Object.create(item);

      /**
       * Gets or sets the value of the select element.
       * @param  {String} the value to set, if undefined, it yields the value.
       * @return {String | HUGEItem} if used as setter, yields the item again,
       *   if used as getter yields the value.
       */
      function value(val) {
        var index;
        var isGetter = arguments.length === 0;

        function selectItemValue() {
          if (isGetter) {
            index = item.getValue();
            if (index >= 0 && typeof index === "number") {
              return item.getOptions()[index].value;
            } else {
              return index;
            }
          } else {
            index = item.getOptions().reduceRight(function (foundIndex, option, i) {
              if (foundIndex === -1 && option.value === val) {
                return i;
              } else {
                return foundIndex;
              }
            }, -1);

            if (index >= 0) {
              item.setValue(index);
            }

            return item;
          }
        }

        if (item.getType() === "select") {
          return selectItemValue();
        } else if (isGetter) {
          return item.getValue();
        } else {
          item.setValue(val);
          return item;
        }
      }

      /**
       * Sets the options in a select element. It automatically reads the current value
       * of the option and leaves it set to the current value if possible.
       * @param  {[type]}
       * @return {[type]}
       */
      function options(optionsArray) {
        var val = value();

        item.setOptions(
          optionsArray.map(function (option) {
            return new Option(option.label, option.value);
          })
        );

        value(val);

        return item;
      }

      function show() {
        function showRecursively(item, prefix, padding) {
          console.log(padding + item.getName().replace(prefix, "") + "    [" +
            [item.getType(), item.value()].filter(function (val) {return !!val;}).join("; ") +
            "]");
          item.getChildren().forEach(function (child) {
            showRecursively(ts(child), item.getName(), padding + "    ");
          });
        }

        showRecursively(item, "", "");
      }

      function find(itemName) {
        function findRecursively(item, found) {
          if (item.getName().split("/").pop().indexOf(itemName) > -1) {
            found.push(TeamsiteHelper(item));
          }
          item.getChildren().forEach(function (child) {
            findRecursively(child, found);
          });

          return found;
        }

        return findRecursively(item, []);
      }

      item.options = options;
      item.value = value;
      item.show = show;
      item.find = find;

      return item;
    }

    function TeamsiteHelper(element) {
      var item;
      if (typeof element === "string") {
        item = IWDatacapture.getItem(element);
      } else {
        item = element;
      }

      if (!item) {
        throw new Error("Could not create a teamsite datacapture element from: " + element);
      }

      return extend(item);
    }

    window.ts = TeamsiteHelper;

    return TeamsiteHelper;
  }
);
