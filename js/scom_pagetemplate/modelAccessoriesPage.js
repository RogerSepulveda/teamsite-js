IWEventRegistry.addFormHandler("onFormInit", function () {
  require([
      "vdapi",
      "metadataDefaultHandle",
      "scom_pagemodules/pageModules",
      "TeamsiteHelper"
    ],
    function (vdapi, mdh, pageModules, ts) {
      "use strict";

      vdapi.series().done(function (data) {
        var options = data.series.map(function (serie) {
          return {
            label: [serie.display_year, serie.series_name].join(" "),
            value: serie.series_code
          };
        });

        ts("/model_accessories_page/MetaData/model_ref").options(options);

        pageModules({
          model: "/model_accessories_page/MetaData/model_ref",
          year: "/model_accessories_page/MetaData/model_year",
          tab: "/model_accessories_page/PageContent",
          itemChange: mdh(data.series),
          onSave: function modulesVerification() {
            if (ts("/model_accessories_page/PageContent/content_blocks").getChildren().length > 1) {
              setTimeout(function () {
                alert("You can only add one module in this page.");
              }, 10);
              return false;
            }

            return true;
          }
        });
      });
    }
  );
});