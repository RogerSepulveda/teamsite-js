require(
    ["vdapi", "jquery"],
    function(vdapi, $) {
        var models = {
            data: null,
            fields: {
                model: "/standard_models/PageData/model_grouping/models/model",
                model_ref: function(instanceName) {
                    return instanceName + "/model_ref";
                },
                vdapi_msrp_ref: function(instanceName) {
                    return instanceName + "/vdapi_msrp_ref";
                },
                model_dcr: function(instanceName) {
                    return instanceName + "/model_dcr";
                },
                dcr_ref: function(instanceName) {
                    return instanceName + "/model_dcr_ref";
                },
            }
        };

        IWEventRegistry.addItemHandler(models.fields.model, "OnReplicantAdded", handleReplicantAdded);
        IWEventRegistry.addItemHandler("/standard_models/PageData/model_grouping/", "OnReplicantAdded", handleReplicantAdded);
        IWEventRegistry.addItemHandler(models.fields.model + "/model_ref", "onItemChange", setModelBaseReferences);
        IWEventRegistry.addItemHandler("/standard_models/PageData/model_grouping/title", "onItemChange", trimSpace);
        IWEventRegistry.addItemHandler("/standard_models/PageData/future_concept_group/title", "onItemChange", trimSpace);
        IWEventRegistry.addItemHandler("/standard_models/PageData/future_concept_group/models/model/non_production_name", "onItemChange", trimSpace);
        IWEventRegistry.addItemHandler("/standard_models/PageData/future_concept_group/models/model/model_image_path", "onItemChange", trimSpace);
        IWEventRegistry.addItemHandler("/standard_models/PageData/project_cars/title", "onItemChange", trimSpace);
        IWEventRegistry.addItemHandler("/standard_models/PageData/project_cars/cta_text", "onItemChange", trimSpace);
        IWEventRegistry.addItemHandler("/standard_models/PageData/project_cars/cta_url", "onItemChange", trimSpace);
        IWEventRegistry.addItemHandler("/standard_models/PageData/project_cars/small_images/image_path", "onItemChange", trimSpace);
        IWEventRegistry.addItemHandler("/standard_models/PageData/project_cars/large_image/image_path", "onItemChange", trimSpace);
        IWEventRegistry.addItemHandler("/standard_models/PageData/banner/title", "onItemChange", trimSpace);

        vdapi.series().done(function(data) {
            models.data = data.series;

            var modelGrouping = IWDatacapture.getItem("/standard_models/PageData/model_grouping");
            modelGrouping.getChildren().forEach(function(item) {
                item.getChildren().filter(function(child) {
                    return child.getType() === "andcontainer";
                }).map(function(child) {
                    child.getChildren().forEach(function(child) {
                        child.getChildren().filter(function(child) {
                            return child.getType() === "select";
                        }).forEach(function(child) {
                            loadModels(child.getName());
                        })
                    })
                })
            });
        });

        function setModelBaseReferences(item) {
            console.log("Inside setModelBaseReferences");
            $.each(models.data, function(idx, series) {
                console.log(item.getOptions()[item.getValue()].value);
                console.log(series.series_code);
                if (item.getOptions()[item.getValue()].value == series.series_code) {
                    var containerName = item.getName().replace("/model_ref", "");
                    console.log(containerName);
                    IWDatacapture.getItem(models.fields.dcr_ref(containerName)).setValue(encodeURI("templatedata/SComVehiclesData/Model/data/" + series.year + "/" + series.series_code + ".xml"));
                }
            });
        }

        function handleReplicantAdded(item) {
            console.log("handleReplicantAdded");

            loadModels(models.fields.model_ref(item.getName()));
            return true;
        }

        function loadModels(itemName) {
            console.log("loadModels: " + itemName);
            var list = IWDatacapture.getItem(itemName);
            var options = list.getOptions();
            var value;
            if (options.length == 1) {
                value = options[0].value;
            }

            var values = [];
            var index;
            $.each(models.data, function (idx, series) {
                var selected = value === series.series_code;
                if (selected) {
                    index = idx;
                }
                values.push(new Option(series.display_year + " " + series.series_name, series.series_code, '', selected));
            });
            list.setOptions(values);
            if (index) {
                list.setValue(index);
            }
        }

        function trimSpace(item) {
            item.setValue(item.getValue().trim());
        }
    }
);



