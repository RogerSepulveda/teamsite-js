IWEventRegistry.addFormHandler("onFormInit", function() {
  require([
      "vdapi",
      "metadataDefaultHandle",
      "scom_pagemodules/pageModules",
      "TeamsiteHelper"
    ],
    function(vdapi, mdh, pageModules, ts) {
      "use strict";

      vdapi.series().done(function(data) {
        var options = data.series.map(function(serie) {
          return {
            label: [serie.display_year, serie.series_name].join(" "),
            value: serie.series_code
          };
        });

        ts("/comparatorPage/MetaData/model_ref").options(options);

        pageModules({
          model: "/comparatorPage/MetaData/model_ref",
          year: "/comparatorPage/MetaData/model_year",
          tab: "/comparatorPage/PageContent",
          itemChange: mdh(data.series),
          requiredModules: ["Compare", "GetYours"],
          onSave: function() {
            var contentBlockContainer = ts("/comparatorPage/PageContent/content_blocks");
            var contentBlocks = contentBlockContainer.getChildren();
            var modulesOrdered = (contentBlocks[0].getChildByName("Compare") != null) && (contentBlocks[1].getChildByName("GetYours") != null);
            if (!modulesOrdered) {
              alert("Comparator page modules order should be \"Compare\" and \"GetYours\". ");
            }
            return modulesOrdered;

          }
        });
      });
    }
  );
});