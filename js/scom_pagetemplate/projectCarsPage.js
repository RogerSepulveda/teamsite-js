require([ 
    "TeamsiteHelper",
    "scom_pagemodules/story_telling/storyTellingModules"
  ],
  function (ts, storyTellingModules) {
    "use strict";

    storyTellingModules({
      tab: "/project_cars_page/PageContent"
    });
  }
);
