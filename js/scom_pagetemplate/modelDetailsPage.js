require([
        "vdapi",
        "accessories",
        "jquery",
        "metadataDefaultHandle",
        "scom_pagemodules/pageModules"
    ],
    function(vdapi, accessories, $, mdh, pageModules) {
        "use strict";

        var cache = {
            selected_series: null,
            series: [],
            features: {}
        };
        var refValue = null;

        vdapi.series().done(function(data) {
            var ref = IWDatacapture.getItem("/model_details_page/MetaData/model_ref");
            var year = IWDatacapture.getItem("/model_details_page/MetaData/model_year");
            var slug = IWDatacapture.getItem("/model_details_page/MetaData/model_slug");

            if (ref.getOptions().length === 1) {
                refValue = ref.getOptions()[0].value;
            }

            data.series.forEach(function(serie) {
                var selected = (serie.series_code === refValue);
                var opt = new Option(
                    [serie.display_year, serie.series_name].join(" "),
                    serie.series_code,
                    selected,
                    selected
                );

                ref.addOption(opt);
            });

            pageModules({
                model: ref.getName(),
                year: year.getName(),
                tab: "/model_details_page/PageContent",
                itemChange: mdh(data.series),
                onSave: function() {
                    var icons = ts("/model_gallery_page/PageModules/content_blocks/content_blocks").find("icons");
                    if (icons.length == 1) {
                        alert("Icon details or editorial item is required for option list module");
                    }

                    return icons.length > 1 || icons.length == 0;
                }
            });

            cache.series = data.series;

            $.each(cache.series, function(index, series) {
                var params = {
                    "series_code": series.series_code,
                    "year": series.year
                };

                vdapi.features(params).done(function(data) {
                    if (data && data[0] && data[0].features) {
                        cache.features[series.series_code] = data[0].features;

                        if (cache.selected_series) {
                            IWDatacapture.getItem("model_details_page/HeroModule/specifications")
                                .getChildren().forEach(function(item) {
                                    item.getChildren().forEach(function(item) {
                                        var ref = item.getChildByName("spec_ref");

                                        populateFeatures(ref);
                                    })
                                });
                        }
                    }
                });

                //accessories.load(series, "/model_details_page/PageContent/content_block", selected);
            });

            var modelChange = function(item) {
                console.log("self.clearDisclaimers");

                var series = data.series.filter(function findSerie(s) {
                    return s.series_code === item.getOptions()[item.getValue()].value;
                });

                if (series.length) {
                    var serie = series[0];
                    year.setValue(serie.year.toString());
                    slug.setValue(serie.series_code);
                    cache.selected_series = serie.series_code;
                }

                populateDisclaimers();
                clearFeatures();
                clearContentBlocks();
            };

            var handleReplicantAdded = function(item) {
                console.log("handleReplicantAdded: " + item.getName());
                populateFeatures(item.getChildByName("spec_ref") || item.getChildByName("feature_ref"));
                return true;
            };

            //IWEventRegistry.addItemHandler("/model_details_page/MetaData/model_ref", "onItemChange", modelChange);
            IWEventRegistry.addItemHandler("/model_details_page/HeroModule/specifications/specification", "OnReplicantAdded", handleReplicantAdded);
        });

        var clearDisclaimers = function() {
            console.log("self.clearDisclaimers");
        };

        var clearFeatures = function() {
            console.log("self.clearFeatures");
            var container = IWDatacapture.getItem("/model_details_page/HeroModule/specifications[1]/specification");

            if (container != null) {
                for (var index = container.getChildren().length; index > 0; index--) {
                    container.deleteInstance(index);
                }
            }
        };

        var populateDisclaimers = function() {
            console.log("self.populateDisclaimers");
        };

        var populateFeatures = function(ref) {
            console.log("self.populateFeatures: " + ref.getName());

            var options = [];
            var selected;

            if (!cache.features[cache.selected_series]) {
                return;
            }
            $.each(cache.features[cache.selected_series], function(index, feature) {
                if (ref.getOptions().length === 1) {
                    var value = ref.getOptions()[0].value;

                    if (feature.name === value) {
                        selected = index;
                    }
                }

                var opt = new Option(
                    feature.name,
                    feature.name,
                    "",
                    ""
                );

                options.push(opt);
            });

            ref.setOptions(options);
            if (selected) {
                ref.setValue(selected);
            }
        }
    }
);