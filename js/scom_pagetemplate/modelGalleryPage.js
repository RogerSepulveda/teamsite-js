IWEventRegistry.addFormHandler("onFormInit", function() {
  require([
      "vdapi",
      "metadataDefaultHandle",
      "scom_pagemodules/pageModules",
      "TeamsiteHelper"
    ],
    function(vdapi, mdh, pageModules, ts) {
      "use strict";

      vdapi.series().done(function(data) {
        var options = data.series.map(function(serie) {
          return {
            label: [serie.display_year, serie.series_name].join(" "),
            value: serie.series_code
          };
        });

        ts("/model_gallery_page/MetaData/model_ref").options(options);

        pageModules({
          model: "/model_gallery_page/MetaData/model_ref",
          year: "/model_gallery_page/MetaData/model_year",
          tab: "/model_gallery_page/PageModules",
          itemChange: mdh(data.series),
          onSave: function() {
            var icons = ts("/model_gallery_page/PageModules/content_blocks/content_blocks").find("icons");
            if (icons.length  == 1) {
              alert("Icon details or editorial item is required for option list module");
            }

            return icons.length > 1 || icons.length == 0;
          }
        });
      });
    }
  );
});