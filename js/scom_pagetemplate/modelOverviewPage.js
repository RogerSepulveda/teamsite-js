IWEventRegistry.addFormHandler("onFormInit", function() {
  "use strict";

  require([
      "vdapi",
      "metadataDefaultHandle",
      "scom_pagemodules/pageModules",
      "TeamsiteHelper"
    ],
    function(vdapi, mdh, pageModules, ts) {
      vdapi.series().done(function(data) {
        var model = ts("/modelOverviewPage/MetaData/model_ref");
        var year = ts("/modelOverviewPage/MetaData/model_year");
        var options = data.series.map(function(serie) {
          return {
            label: [serie.display_year, serie.series_name].join(" "),
            value: serie.series_code
          };
        });

        function fillHeroSpecs() {
          vdapi.features({
            "series_code": model.value(),
            "year": year.value()
          }).done(function(features) {
            if (Array.isArray(features)) {
              var options = features
                .map(function(features) {
                  return features.features.map(function(feature) {
                    return {
                      label: feature.name,
                      value: feature.name
                    };
                  });
                }).reduce(function(carry, item) {
                  return carry.concat(item);
                }, []);

              ts("modelOverviewPage/HeroModule").find("spec_name_ref")
                .forEach(function(specRef) {
                  specRef.options(options);
                });
            }
          });
        }

        ts("/modelOverviewPage/MetaData/model_ref").options(options);
        setTimeout(fillHeroSpecs, 100);

        pageModules({
          model: model.getName(),
          year: year.getName(),
          tab: "/modelOverviewPage/PageContent",
          requiredModules: ["Compare", "Series", "Visualizer"],
          itemChange: (function() {
            var hiddenFieldsHandle = mdh(data.series);

            return function changeHandle(item) {
              hiddenFieldsHandle(item);
              fillHeroSpecs();
            };
          }()),
          onSave: function() {
            var iconDetailsPresent = true;
            var icons = ts("/modelOverviewPage/PageContent/content_blocks/content_blocks").find("icons");
            if (icons.length == 0) {
              return true;
            }

            var iconsFiltered = icons.filter(function(icon) {
              return icon.getName().endsWith("icons");
            });            

            iconsFiltered.forEach(function(icon) {
              iconDetailsPresent = iconDetailsPresent && (icon.getChildByName("icon_details[1]") !== undefined || icon.getChildByName("editorial_item[1]") !== undefined);
            });
            if (!iconDetailsPresent) {
              alert("Icon details or editorial item is required for option list module");
            }

            return iconDetailsPresent;
          }
        });
      });
    });
});