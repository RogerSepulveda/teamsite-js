define([], function () {
  "use strict";
  var D = IWDatacapture;

  return function (seriesData) {
    return function metadata(item) {
      var serie;
      var series = seriesData.filter(function findSerie(s) {
        return s.series_code === item.getOptions()[item.getValue()].value;
      });
      if (series.length) {
        serie = series[0];
        D.getItem(item.getName() + "/../model_year").setValue(serie.year.toString());
        D.getItem(item.getName() + "/../model_slug").setValue(serie.series_code);
      }
    };
  };
});
