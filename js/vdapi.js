define(["jquery"],
  function ($) {
    "use strict";
    var vdapi = {};
    var vdapiurl = "http://next.staging.ws.toyota.com/CLSREST/api/vehicle_data";
    var defaults = {
      brand: "scion",
      key: "MtK7p8aB9qmCBObkKlzs"
    };

    vdapi.series = function (params) {
      return $.ajax({
        url: vdapiurl + "/series",
        type: "GET",
        dataType: "JSONP",
        data: $.extend({}, defaults, params)
      });
    };
    
    vdapi.features = function (params) {
      return $.ajax({
        url: vdapiurl + "/features",
        type: "GET",
        dataType: "JSONP",
        data: $.extend({}, defaults, params)
      });
    };

    vdapi.accessories = function (params) {
      return $.ajax({
        url: vdapiurl + "/accessories",
        type: "GET",
        dataType: "JSONP",
        data: $.extend({}, defaults, params)
      });
    }; 

    vdapi.warranties = function (params) {
      return $.ajax({
        url: vdapiurl + "/warranties",
        type: "GET",
        dataType: "JSONP",
        data: $.extend({}, defaults, params)
      });
    }; 

    return vdapi;
  }
);

