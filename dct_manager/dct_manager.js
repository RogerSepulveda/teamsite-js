// create a fake console if one doesn't exist
(function initConsole() {
    if (typeof (window.console) !== "object") {
        var names = ["log", "debug", "info", "warn", "error", "assert", "dir", "dirxml", "group", "groupEnd", "time", "timeEnd", "count", "trace", "profile", "profileEnd"];
        window.console = {};
        for (var i = 0; i < names.length; ++i) window.console[names[i]] = function () { };
    }
} ());

var SCION = (function ($, scion) {
		
	/**
	 * Global object literal holding constants.
	 * 
	 * @author Huge
	 * @version 1.0
	 */
	scion.Constants = {
		SERVICES : {
			MAKE_PATH : "/jsp/makePath.jsp",
			CONSUME_DCR: "/jsp/consumeDCR.jsp",
			BRANCH_DEV: "SCION_Admin/WORKAREA",
			BRANCH_QA: "SCION_QA/WORKAREA",
			BRANCH_PREVIEW: "Scion_Authoring/WORKAREA",
			PREVIEW_URL_DEV: "http://scion-web-development.elasticbeanstalk.com",
			PREVIEW_URL_QA: "http://scion-web-development.elasticbeanstalk.com",
			PREVIEW_URL_PREVIEW: "http://scion-web-development.elasticbeanstalk.com",
			CSSDK_SERVER: "/scion-cssdk"
		},		
		MESSAGES : {
			SAVING_DCR : "Saving DCR and Deploying Content..."
		}
	};

	/**
	 * Common functions.
	 * 
	 * @author Huge
	 * @version 1.0
	 */
	scion.Utilities = {

		/**
		* Read list of Extended Attributes to be externalized per datatype
		*/

		GetDCTConfig : function(_contentCategory, _contentType){
			var dcrName = "";
			var dcrNameFormat = "";
			var eaList = new Object();
			var dcrNameFields = Array();

			console.log("Making AJAX call...");
			$.ajax({
				 type: "GET",
				 url: "/iw/scion_redesign/dct_manager/dct_manager.xml",
				 dataType: "xml",
				 async: false,
				 success: function(xml){
					$(xml).find("category").each(function(){
						//var categoryName = $(this).find("name").first().text();
						var categoryName = $(this).attr("name");
						if (categoryName == _contentCategory) {
							console.log("category found : " + categoryName);
							var ccObj = this;
							$(ccObj).find("data-type").each(function() {
								var ctObj = this;
								// var contentTypeName = $(ctObj).find("name").first().text();
								var contentTypeName = $(ctObj).attr("name");
								if (contentTypeName == _contentType) {
									console.log("data-type found: " + contentTypeName);
									var etObj = this;
									// get EA parameters
									$(etObj).find("eaTuple").each(function() {
										var tupObj = this;
										var eaName = $(tupObj).find("dcrEA").first().text();
										var eaXPath = $(tupObj).find("dcrFormPath").first().text();
										console.log("Adding pair : " + eaName + " - " + eaXPath);
										eaList[eaName] = eaXPath; 
									});
									// get DCR Name parameters
									 dcrNameFormat = $(etObj).find("dcrNameFormat").text();
									console.log("dcrNameFormat : " + dcrNameFormat);
									$(etObj).find("formPath").each(function() {
										var nameFieldObj = this;
										var formPath = $(this).text();
										dcrNameFields.push(formPath);
										console.log("formPath : " + formPath);
									});
								}
							});	
						}
					});			 
				 }
			});
			return [dcrNameFormat, dcrNameFields, eaList];
		},

		/**
		 * Atempts to find an item without need for specifying the root container/tab
		 *
		 */	
		FindItem : function(path){
			// Makes sure path begins with a /
			path = scion.Utilities.EnsurePrefix(path);
			
			// Find the DCT root element
			// Custon DCTs always return one element because there may be only one top-level node
			var root = IWDatacapture.getRootItems()[0]; 
			// check for item under the root
			var checkPath = root.itemImpl.getXPath() + path;
			var item = IWDatacapture.getItem(checkPath);
			
			// if item wasn't found, check for item under each tab
			if (item === null){
				var tabs = root.getChildren();
								
				for (tab in tabs) {
					var tabCheckPath = tabs[tab].itemImpl.getXPath() + path;
					item = IWDatacapture.getItem(tabCheckPath);
					
					if (item !== null) { break; }
				}
			}
			return item;
		},
		
		/**
		 * Atempts to find the full XPath of an item without need for specifying the root container/tab
		 *
		 */	
		FindItemXPath : function(path){
			var item = scion.Utilities.FindItem(path);
			
			if (item !== null) {
				return item.itemImpl.getXPath();
			}
			
			return undefined;
		},
		
		
		EnsurePrefix : function(path) {
			// add the '/' prefix if not present in the path
			var prefix = (path.substr(0, 1) == '/') ? '' : '/';
			return prefix + path;
		},
	
		/**
		 * Fetches the value of a given teamsite form field
		 *
		 * @param path XPath of target field
		 */		
		GetValueFromRelativePath : function(path){
			path = scion.Utilities.EnsurePrefix(path);
			path = scion.Utilities.FindItemXPath(path);
			
			var item = IWDatacapture.getItem(path);
			
			if (item !== null && item.itemImpl !== null) {
				var value = item.itemImpl.getValue();
				return value;
			} 						
			return undefined;	
		},

		/**
		 * Fetches the value of a given teamsite form field
		 *
		 * @param path XPath of target field
		 */		
		GetValueFromPath : function(path){
			path = scion.Utilities.EnsurePrefix(path);
			
			var item = IWDatacapture.getItem(path);
			
			if (item !== null && item.itemImpl !== null) {
				var value = item.itemImpl.getValue();
				return value;
			} 						
			return undefined;	
		},
	
		/**
		 * Combines multiple fields values in the given format
		 *
		 * @param format A string containing numbered placeholders {n}
		 * @param fieldPaths An array of XPaths of the fields
		 */	
		FormatFields : function(format, fieldPaths){
			var fieldValues = [];
			
			// find the value of each field and store it for later application to formatted string
			for (path in fieldPaths) {
				var value = scion.Utilities.GetValueFromPath(fieldPaths[path]);
				fieldValues.push(value);
			}
			
			var s = format,
				i = fieldValues.length;
			
			// replace each placeholder with the coresponding value
			while (i--) {
				s = s.replace(new RegExp('\\{' + i + '\\}', 'gm'), fieldValues[i]);
			}
			
			return s;					
		},
		
		/**
		 * Combines multiple fields values in the given format. 
		 * NOTE: Variation of FormatFields, used only to generate DCR name.
		 *
		 * @param format A string containing numbered placeholders {n}
		 * @param fieldPaths An array of XPaths of the fields
		 */	
		FormatNameFields : function(format, fieldPaths, allowCaps, spaceReplacementChar){
			var fieldValues = [];
			
			allowCaps = allowCaps || false;
			spaceReplacementChar = spaceReplacementChar || "-";
			
			if (fieldPaths !== undefined) {
				// find the value of each field and store it for later application to formatted string
				for (path in fieldPaths) {
					console.log("Getting value from :" + fieldPaths[path]);
					var value = scion.Utilities.GetValueFromRelativePath(fieldPaths[path]);
					console.log("Value :" + value);
					
					if ((!allowCaps) && (value !== undefined)){
						value = value.toLowerCase();
					}
					
					value = $.trim(value);
					value = value.replace(/\s/g, spaceReplacementChar); // substitute all spaces with dashes
					value = value.replace(/[^\w\.\-]/g, ""); // strip all non-URL characters (Unix)
					
					fieldValues.push(value);
				}
			}
			
			var s = format,
			i = fieldValues.length;
			
			// replace each placeholder with the coresponding value
			while (i--) {
				s = s.replace(new RegExp('\\{' + i + '\\}', 'gm'), fieldValues[i]);
			}

			s = s.replace(/__/g, '_');
			s = s.replace('_\.xml', '\.xml');
			
			return s;						
		},
	
		/**
		 * Disables subsequent modification of a field once the DCR has been saved
		 *
		 * @param fieldPath The XPath of the field
		 */	
		LockIfSaved : function(fieldPath){
			// Check if the DCR is named (has been saved).
			var isDCRNamed = IWDCRInfo.getDCRName() || false;
			if (isDCRNamed){
				var field = IWDatacapture.getItem(fieldPath);
				field.setReadOnly(true);
			}			
		},
				
		/**
		 * Recursively goes through all containers and its replicants and return the list of items.
		 * 
		 * @param targetPath XPath for which we are searching the items
		 * @param parentPath parent path prefix
		 * @return all items that exist ant given XPath
		 */
		GetAllItemsAtPath : function(targetPath, parentPath){
			var itemList = [];
		
			parentPath = parentPath || "";
			
			// ensure path prefix (item path must start with '/')
			targetPath = scion.Utilities.EnsurePrefix(targetPath);
		
			// Check the targetPath for instances of '[]' which indicates a replicant container
			var repIndicatorIndex = targetPath.indexOf("[]");
			if (repIndicatorIndex > -1 ) {
				// There is at least one replicant container in the targetPath. Recursively call this method 
				// on all existing replicant instances in the container 
				
				var replicantContainerPath = targetPath.substring(0, repIndicatorIndex); // the current replicant container
				var remainingPath = targetPath.substring(repIndicatorIndex + 2);    // the remainder of the XPath relative to the replicant container; unchecked for additional replicant containers
				
				var replicantContainer = IWDatacapture.getItem(parentPath + replicantContainerPath);
				
				if (replicantContainer != null) {
					var replicantInstances = replicantContainer.getChildren();
								
					// wire up all existing replicant instances
					$.each(replicantInstances, function(index, replicantInstance){
						var parent = replicantInstance.itemImpl.getXPath(); // the current replicant instance becomes the parent
						itemList = itemList.concat(SCION.Utilities.GetAllItemsAtPath(remainingPath, parent));
					});
				}
				
			} else {
				// No replicants found in the targetPath; we should have reached an IWItem
				var targetItem = IWDatacapture.getItem(parentPath + targetPath);
				
				if (targetItem != null) {				
					itemList.push(targetItem);	// add target item to array
				}
			}
			
			return itemList;
		}
	};
	
	/**
	 * Handles the addition of DCT fields into extended attributes
	 * 
	 * @author Huge
	 * @version 1.0
	 */
	scion._extendedAttributeManager = function() {

		// private
		var _self = this; // 'self' is a helper variable which will be used instead of 'this' when we are out of the scope
		var _attributeQueue = {};
		this.attributesModified = false;

		/**
		 * Sets an extended attribute as the value of a Teamsite form field
		 *
		 * @param attributeName Full name of the extended attribute; will appear as Teamsite/metadata/{attributeName}
		 * @param fieldPath XPath of target field
		 * @param forceAdd boolean; if true, allows a parameter to be set (as an empty string) if the field cannot be found
		 */	
		this.AddField = function(attributeName, fieldPath, forceAdd){
			if (fieldPath !== undefined) {
				IWEventRegistry.addItemHandler(scion.Utilities.EnsurePrefix(fieldPath), "onItemChange", _self._attributeModified, false);
			}
			
			_attributeQueue[fieldPath] = { 
				"attributeName":attributeName,
				"forceAdd": forceAdd,
				"modified": false
			};
		};
		
		/**
		 * Sets modified attribute for given exteneded attribute.
		 * 
		 * @param item IWItem for which we are setting the modified attribute
		 */
		this._attributeModified = function(item) {
			_self.attributesModified = true;
			
			var fieldPath = item.itemImpl.getXPath();
			if (_attributeQueue[fieldPath] !== undefined){
				_attributeQueue[fieldPath].modified = true;
			}
		};
			
		/**
		 * Returns an array of name/value pairs for queued extended attributes.
		 *
		 * @param attributeName Full name of the extended attribute
		 */		
		this._processQueue = function(){
			var attributeValues = [];
			
			$.each(_attributeQueue, function(key, properties) {
				var value = _self._processAttribute(key, properties);
				
				if (value !== undefined) {
					attributeValues.push(value);
				}
			});
			
			return attributeValues;
		};
		
		/**
		 * Creates an extended attribute from a Teamsite form field.
		 *
		 * @param fieldPath XPath of target field
		 * @param attributeName Full name of the extended attribute, optional
		 * @return undefined
		 */	
		this._processAttribute = function(fieldPath, properties){
			var attributeName = properties.attributeName;
			var forceAdd = properties.forceAdd;
			var modified = properties.modified
			
			var attributeValue;			
			if (fieldPath !== undefined) {
				 attributeValue = scion.Utilities.GetValueFromPath(fieldPath);
			}
			
			if (forceAdd && attributeValue === undefined){
				attributeValue = "";
			}
			
			if (attributeValue !== undefined){
				return { "name":attributeName, "value":attributeValue, "modified":modified };
			}
			
			return undefined;
		};
			
		/**
		 * Returns a collection of extended attributes
		 *
		 * @return list of extended attributes
		 */	
		this._getAttributesForQueryString = function(){
			// trigger the queue processor to get the most up to date field values
			var _attributeValues = this._processQueue();
			
			var joinedValues = [];
			
			// enumerate through all attribute values
			$.each(_attributeValues, function(index, attribute) {
				joinedValues.push(attribute.name+":"+attribute.value);
			});

			
			return joinedValues;
		};
	};	

	/**
	 * Handles the generation of the DCR name.
	 * 
	 * @param nameTemplate string template that will be used to generate DCR name (e.g. "/{0}/model_{1}.xml")
	 * @param itemPaths array of item paths that will be used to replace template placeholders with item values (e.g. ["Series/series_name", "Series/year"])
	 * @param dctManager instance of SCION.DCTManager object
	 * 
	 * @author Huge
	 * @version 1.0
	 */
	scion._dcrNameGenerator = function(nameTemplate, itemPaths, dctManager){
		
		// private
		var _self = this;
		var _nameTemplate;
		var _itemPaths;
		var _dctManager; // save reference to instance of SCION.DCTManager object
		var _dcrName; // save the original DCR name
		var _contextPath;
		
		// used by SCION.DCTManager object
		this.isLocalizedDCR;
		
		/**
		 * Registers FormAPI "onSaveValid" event handler for selector object. 
		 * Handler is responsible for creating DCR name.
		 * 
		 * @param nameTemplate string template that will be used to generate DCR name (e.g. "{year}_{series_name}.xml")
		 * @param itemPaths array of item paths that will be used to replace template placeholders with item values 
		 * @param dctManager instance of SCION.DCTManager object
		 */
		this._init = function(nameTemplate, itemPaths, dctManager){
			_nameTemplate = scion.Utilities.EnsurePrefix(nameTemplate);
			_itemPaths = itemPaths;
			_dctManager = dctManager;
			_contextPath = top.getDCFrame(self).gContextPath;
		};
				
		/**
		 * Checks whether auto-generated DCR name already exists. If name is already specified set validName to false.
		 * (Items used to generate DCR name have the same value in some other DCR)
		 */
		this._checkStatus = function(){
			console.log("ENTERED _checkStatus...");
			var status = IWDCRInfo.getStatus();
			
			switch(status){
				case IWDCRInfo.PENDING:
					console.log("PENDING...");
					// since we use setTimeout, using return is useless
					setTimeout(function(){_self._checkStatus();}, 300);
					break;
				case IWDCRInfo.AVAILABLE:
					// DCR with given name already exists
					console.log("AVAILABLE...");
					if (IWDCRInfo.fileSize == 0) {
						console.log("The DCR already exists but file is 0 bytes, overwriting");
						
						// save the data (does not generate the onSave, onSaveValid, or onSaveNameSpecified events)
						IWDatacapture.save();
						break;
					} else {
						console.log("The DCR already exists. Do you want to override it?");
						// TODO: use FormAPI to show error message to the user
						alert("The DCR '" + IWDCRInfo.getDCRName() + "' already exists!");
						
						// allow user to set correct DCR name: access datacapture_buttonframe and use dcOperations object to display workarea browser
						IWDCRInfo.setDCRName(null);
						window.top.frames.datacapture_buttonframe.dcOperations.showWorkareaBrowser();
						
						IWEventRegistry.addFormHandler("onSaveValid", function(){return  _self._generateDCRName();}, false);
					}
					break;
				case IWDCRInfo.UNAVAILABLE:
					// DCR with given name does not exist
					console.log("UNAVAILABLE...");
					console.log("New DCR");
					
					// create DCR path if it does not exist
					_self._createDCRPath();
					
					// if everything is ok save the data (does not generate the onSave, onSaveValid, or onSaveNameSpecified events)
					IWDatacapture.save();
					break;
				case IWDCRInfo.ERROR:
					console.log("ERROR...");
					console.log("Can not set DCR Name");
					break;
				default: 
					console.log(status + " - That should not have happened!");
					break;
			}
		};
				
		/**
		 * Synchronously calls server side code which will create directory structure for given DCR file.
		 */
		this._createDCRPath = function(){
			var workarea = IWDatacapture.getWorkarea();
			var dcrPath = IWDatacapture.getDCRPath();
			
			dcrPath = dcrPath.replace(workarea+"/", "").replace(/[^\/]*\.xml$/gi, "");
			
			console.log("dcrPath: " + dcrPath);
			
			// generate directory path dirPath within given workArea
			var requestData = {"workArea" : workarea, "dirPath" : dcrPath};
			
			$.ajax({
			//	url: _contextPath + scion.Constants.SERVICES.MAKE_PATH,
				url: scion.Constants.SERVICES.CSSDK_SERVER + scion.Constants.SERVICES.MAKE_PATH,
				async: false,
				data: requestData,
				success: function(data){
					console.log("Success: DCR path created!");
				}, 
				error: function(xhr, status, error){
					console.log("Error : "+ status);
				}
			});
		};
		
		// call init function upon object creation
		this._init(nameTemplate, itemPaths, dctManager);
	};

	/**
	 * DCT manager controls all common functions nessecary to load a DCT and save a DCR.
	 * 
	 * @author Huge
	 * @version 1.0
	 */
	scion.DCTManager = function(configuration) {
				
		//public	
		this.ExtendedAttributes = null;

		// private
		var _self = this; // 'self' is a helper variable which will be used instead of 'this' when we are out of the scope
		var _configuration = configuration || {};
		var _dcrNameGenerator;
		
		var _formType; // the full formType (dataCategory/dataType) of the current DCT
		var _dataCategory; // the data category of the current DCT
		var _dataType;	// the data type of the current DCT
		var _workarea;	// the work area of the current DCT
		var _domain;	// the domain branch under which this DCT resides
		var _locale; // the language local of the current DCR
		var _datacapture;
		var _contextPath;
		
		var _isPreview = false;
		
		/**
		 * Enables image preview, sets default extended attributes and initializes DCR name generator object.
		 */
		this._init = function(){	
	 		_contextPath = top.getDCFrame(self).gContextPath;
			
			// enable image preview
			IWDatacapture.enableImagePreview(true);
		
			// Find and set some basic information about the current DCT	
			this._getFormType();
								
			// Set Default Extended Attributes
			// create a new instance of the extended attribute manager for public use
			this.ExtendedAttributes = new scion._extendedAttributeManager();
			

			// make any additional category default settings
			switch(_dataCategory) {
				case "modules":
					this._setModuleAttributes();
					break;
				default:
					break;
			}
	
			// Set up DCR Name Generator
//			this._setDCRNameGenerator();
			
			// Wire Events
			IWEventRegistry.addFormHandler("onFormInit", this._onFormInit, false);
			IWEventRegistry.addFormHandler("onSave", this._onSave, false);		
			IWEventRegistry.addFormHandler("onSaveValid", this._onSaveValid, false);				
			IWEventRegistry.addFormHandler("onSaveDone", this._onSaveDone, false);
		};	
				
		/**
		 * Synchronously calls server side code which will create directory structure for given DCR file.
		 */
		this._createDCRPath = function(){
			var workarea = IWDatacapture.getWorkarea();
			var dcrPath = IWDatacapture.getDCRPath();
			
			dcrPath = dcrPath.replace(workarea+"/", "").replace(/[^\/]*\.xml$/gi, "");
			
			console.log("dcrPath: " + dcrPath);
			
			// generate directory path dirPath within given workArea
			var requestData = {"workArea" : workarea, "dirPath" : dcrPath};
			
			$.ajax({
			//	url: _contextPath + scion.Constants.SERVICES.MAKE_PATH,
				url: scion.Constants.SERVICES.CSSDK_SERVER + scion.Constants.SERVICES.MAKE_PATH,
				async: false,
				data: requestData,
				success: function(data){
					console.log("Success: DCR path created!");
				}, 
				error: function(xhr, status, error){
					console.log("Error : "+ status);
				}
			});
		};
		
		/**
		 * Sets up the DCR Name Generator.
		 */	
		this._setDCRNameGenerator = function(){	
			console.log("Setting DCR name using format :" + _configuration.dcrNameFormat);
			if (_configuration.dcrNameFormat !== undefined){
				_dcrNameGenerator = new scion._dcrNameGenerator(_configuration.dcrNameFormat, _configuration.dcrNameFields, _self);
			}
		};
		
		/**
		 * Sets dirty bit in order for consumeDCR.jsp to be triggered.
		 */
		this._setConsumeDcrTrigger  = function() {
			_datacapture = window.top.frames.formframe._d;
			// set dirty bit to true in order for consumeDCR to be triggered
			_datacapture.setIsModified(true);
		}
	
		
		/**
		 * Show status message.
		 */
		this._showStatusBarMessage = function(message){
			_datacapture = _datacapture || window.top.frames.formframe._d; 
			
			var messagePrefix = "<span style=\"color:red; font-family:Verdana,sans-serif; font-size:14px; padding-left:10px;\">";
			var messageSuffix = "</span>";
			var fullMessage =messagePrefix + message + messageSuffix;
			
			_datacapture.updateStatus(fullMessage, true); 
		}
		
		
		
		/**
		 * Gets the data category and data type of the current DCT.
		 */	
		this._getFormType = function(){
			_formType = IWDatacapture.getFormType(); // Returns the form type as determined by the data_category/data_type directory structure in the templatedata directory
			
			// split up Form Type into Data Type and Data Category
			var result = _formType.split("/");
			_dataCategory = result[0];
			_dataType = result[1];	
		};
	
		/**
		 * Sets the module specific fields as extended attributes.
		 */	
		this._setExtendedAttributes = function(eaList){
			for (var ea in eaList) {
				if (eaList.hasOwnProperty(ea)) {
					console.log(ea + " - " + scion.Utilities.FindItemXPath(eaList[ea]));
					this.ExtendedAttributes.AddField(ea, scion.Utilities.FindItemXPath(eaList[ea]));
				}
			}
		};
		
		/**
		 * Handler for onFormInit event; will populate some common fields
		 */	
		this._onFormInit = function(){
			
			// set dirty bit to true
			_self._setConsumeDcrTrigger();
			
			return true;
		};
		
		/**
		 * Handler for onSave event. Sets _isPreview local variable.
		 * 
		 * @return true
		 */	
		this._onSave = function(button){
			switch (button) {
			 case IWDatacapture.SAVE_BUTTON:
			 case IWDatacapture.SAVEAS_BUTTON:
			 case IWDatacapture.SAVECLOSE_BUTTON:
				 // User clicked 'Save', 'Save As' or 'Finish'
				 _isPreview = false;
				 break;
			 case IWDatacapture.PREVIEW_BUTTON:
			 case IWDatacapture.GENERATE_BUTTON:
				// User clicked 'Preview' or 'Generate'
				_isPreview = true;
				break;
			}			
			return true;
		};
		
		/**
		 * Handler for onSaveValid event; will do some final processing and call consumeDCR.jsp.
		 * 
		 * @return false if we use _dcrNameGenerator to set the DCR name, otherwise it returns false
		 */	
		this._onSaveValid = function(){	    
			// Get the configured EA list for each DCT type
			// invoked onSaveValid because the dcrtmanager has both
			// dcrName config and list on EA that need to be set
			// avoids the need to make a additional ajax call onSaveDone to get EAs
 			console.log("Getting EA lists from file...");
			dctConfig = scion.Utilities.GetDCTConfig(_dataCategory, _dataType);
			var _dcrNameFormat = dctConfig[0];
			var _dcrNameFields = dctConfig[1];
			var eaList = dctConfig[2];
			_self._setExtendedAttributes(eaList);

			_configuration.dcrNameFormat = dctConfig[0];
			_configuration.dcrNameFields = dctConfig[1];

				// Check if the DCR is named (has been saved).
			var isDCRNamed = IWDCRInfo.getDCRName() || false;
			
			var allowCaps = true;
			var spaceReplacementChar = "_";

			var newDCRName = scion.Utilities.FormatNameFields(_dcrNameFormat, _dcrNameFields, allowCaps, spaceReplacementChar);
			console.log("Set DCR Name: " + newDCRName);
			if (isDCRNamed)
			{
				var currentDCRName =  IWDCRInfo.getDCRName();
				console.log("Current DCR Name :" + IWDCRInfo.getDCRName());
				console.log("New DCR Name :" + newDCRName);				
				if (newDCRName != currentDCRName)
				{
					var r=confirm("DCR will be copied to : " + newDCRName);
					if (r==false)
					  {
						return false;
					  } else {
						IWDCRInfo.setDCRName(newDCRName);
						_self._createDCRPath();
					  }
					
				}
			} else {
				IWDCRInfo.setDCRName(newDCRName);
				_self._createDCRPath();
			}

			// if everything is ok save the data (does not generate the onSave, onSaveValid, or onSaveNameSpecified events)
			IWDatacapture.save();	
			return true;
		};
		
		/**
		 * Handler for onSaveDone event; will do some final processing and call consumeDCR.jsp.
		 * 
		 * @return true
		 */	
		this._onSaveDone = function(){	    
			var data = {};
			
			// set the DCR vpath so that metadata fields can be set on the server
			data.vpath = IWDatacapture.getDCRPath();
			
			// set the formated Extended attributes for server side consumption
			data.extendedAttribute = _self.ExtendedAttributes._getAttributesForQueryString();
			
			//data.AttributesModified = _self.ExtendedAttributes.attributesModified;
			data.AttributesModified = true; // defaulting it to true
			data.IsPreview = _isPreview;

			var callAsync = !_isPreview; // If the preview button was pressed, make the consumeDCR call synchronously
			
			_self._startConsumeDCRIndicator(); // If the preview button was pressed, show status message

			// send all data off to consumeDCR.jsp for processing
			var jqxhr = $.ajax({
				"async": callAsync,
			//	"url": _contextPath + scion.Constants.SERVICES.CONSUME_DCR,
				"url": scion.Constants.SERVICES.CSSDK_SERVER + scion.Constants.SERVICES.CONSUME_DCR,
				"data": $.param(data, true), 		// true = use tradtional parameter serialization
				complete : function(data){
					// hide status message
					_self._stopConsumeDCRIndicator();
					if (!this.async) {
						console.log ("Preview button clicked. Save done");
						IWDatacapture.displayMessage("DCR Successfully saved");
						if (typeof(scion.Utilities.FindItem("Metadata/url_slug")) != undefined) {
							if (parent.gWorkarea.indexOf(scion.Constants.SERVICES.BRANCH_DEV) > 0) {
								console.log ("Metadata/url_slug :" + scion.Utilities.FindItem("Metadata/url_slug"));
								console.log ("Preview URL :" + scion.Constants.SERVICES.PREVIEW_URL_DEV + scion.Utilities.GetValueFromRelativePath("Metadata/url_slug"));
								window.open (scion.Constants.SERVICES.PREVIEW_URL_DEV + scion.Utilities.GetValueFromRelativePath("Metadata/url_slug"), "_blank");
							}   else if (parent.gWorkarea.indexOf(scion.Constants.SERVICES.BRANCH_QA) > 0) {
								console.log ("Metadata/url_slug :" + scion.Utilities.FindItem("Metadata/url_slug"));
								console.log ("Preview URL :" + scion.Constants.SERVICES.PREVIEW_URL_QA + scion.Utilities.GetValueFromRelativePath("Metadata/url_slug"));
								window.open (scion.Constants.SERVICES.PREVIEW_URL_QA + scion.Utilities.GetValueFromRelativePath("Metadata/url_slug"), "_blank");
							}   else if (parent.gWorkarea.indexOf(scion.Constants.SERVICES.BRANCH_PREVIEW) > 0) {
								console.log ("Metadata/url_slug :" + scion.Utilities.FindItem("Metadata/url_slug"));
								console.log ("Preview URL :" + scion.Constants.SERVICES.PREVIEW_URL_PREVIEW + scion.Utilities.GetValueFromRelativePath("Metadata/url_slug"));
								window.open (scion.Constants.SERVICES.PREVIEW_URL_PREVIEW + scion.Utilities.GetValueFromRelativePath("Metadata/url_slug"), "_blank");
							}
						} else {
							console.log ("URL slug not defined for preview under DCT Manager");
							IWDatacapture.displayMessage("URL slug not defined for preview under DCT Manager");
						}
						return false;
					} else {
						console.log ("Save button clicked. Save done");
						IWDatacapture.displayMessage("DCR Successfully saved and deployed to LSCS");
					}
				}
			});
			
			return true;
		};
		
		/**
		 * Show status message with loading indicator.
		 */
		this._startConsumeDCRIndicator = function(){
			// need to check if _datacapture is defined, since SCION.DCTManager is not created when "onFormInit" is triggered
			_datacapture = _datacapture || window.top.frames.formframe._d; 
			
			_datacapture.updateStatusGif(_contextPath + _datacapture.strings.image_busy_icon, true);
			_datacapture.updateStatus(scion.Constants.MESSAGES.SAVING_DCR, true); 
		}
		
		/**
		 * Hide status message.
		 * We can use bulit in FormAPI function.
		 */
		this._stopConsumeDCRIndicator = function(){
			IWDatacapture.displayMessage();
		}
		
		// call init function upon object creation
		this._init();			
	};
		
	return scion;

})(jQuery, SCION || {});
